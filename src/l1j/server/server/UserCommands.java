package l1j.server.server;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.RobotSystem.RobotAIThread;
import l1j.server.server.command.L1Commands;
import l1j.server.server.command.executor.L1CommandExecutor;
import l1j.server.server.datatables.CharacterTable;
//import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.monitor.LoggerInstance;
import l1j.server.server.serverpackets.S_Ability;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_HPMeter;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_OwnCharStatus2;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_Unknown2;
import l1j.server.server.serverpackets.S_UserCommands4;
import l1j.server.server.serverpackets.S_UserCommands5;
import l1j.server.server.serverpackets.S_War;
import l1j.server.server.templates.L1Command;
import l1j.server.server.utils.SQLUtil;
import manager.LinAllManager;

public class UserCommands {

    private static Logger _log = Logger.getLogger(UserCommands.class.getName());

    boolean spawnTF = false;

    private static UserCommands _instance;

    private static Random _random = new Random(System.nanoTime());

    private UserCommands() {
    }

    public static UserCommands getInstance() {
	if (_instance == null) {
	    _instance = new UserCommands();
	}
	return _instance;
    }

    /**
     * @param pc
     * @param cmdLine
     */
    public void handleCommands(L1PcInstance pc, String cmdLine) {
	if (pc == null) {
	    return;
	}
	// System.out.println(cmdLine);
	StringTokenizer token = new StringTokenizer(cmdLine);
	// System.out.println(token.hasMoreTokens());
	String cmd = "";
	if (token.hasMoreTokens())
	    cmd = token.nextToken();
	else
	    cmd = cmdLine;
	String param = "";
	// System.out.println(cmd);

	while (token.hasMoreTokens()) {
	    param = new StringBuilder(param).append(token.nextToken()).append(' ').toString();
	}
	param = param.trim();

	// Databaseized commands
	if (executeDatabaseCommand(pc, cmd, param)) {
	    return;
	}

	try {
	    switch (cmd) {
	    case "help":
		showHelp(pc);
		break;
	    case "tellag":
	    case ".":
		tell(pc);
	    case "autoclanjoin":
		autoclanjoin(pc);
		break;
	    case "info":
		check(pc);
		break;
	    case "statusreset":
		statusInitialization(pc);
		break;
	    case "location":
		location(pc);
		break;
	    case "age":
		age(pc, param);
		break;
	    case "hunt":
		Hunt(pc, param);
		break;
	    case "clanpt":
		BloodParty(pc);
		break;
	    case "secure":
		changequiz(pc, param);
		break;
	    case "unsecure":
		validateQuiz(pc, param);
		break;
	    case "changepass":
		changepassword(pc, param);
		break;
	    case "dropcomment":
	    case "comment":
		Ment(pc, param);
		break;
	    case "privateshop":
		privateShop(pc);
		break;
	    case "privateshop1":
		privateShop1(pc);
		break;
	    case "namechange":
	    case "changename":
		changename(pc, param);
	    case "light":
	    case "maphack":
		maphack(pc, param);
		break;
	    case "phone":
		phone(pc, param);
		break;
	    case "mark1":
		Mark1(pc, param);
		break;
	    case "doll":
		POPall(pc);
		break;
	    case "killranking":
		pc.sendPackets(new S_UserCommands4(pc, 1));
		break;
	    case "deathranking":
		pc.sendPackets(new S_UserCommands5(pc, 1));
		break;
	    case "piva":
		execute(pc, param, param);
		break;
	    case "abysspoint":
		String grade = "";

		switch (pc.getPeerage()) {
		case 0:
		    grade = "Apprentice";
		    break;
		case 1:
		    grade = "9th Soldier";
		    break;
		case 2:
		    grade = "8th Soldier";
		    break;
		case 3:
		    grade = "7th Soldier";
		    break;
		case 4:
		    grade = "6th Soldier";
		    break;
		case 5:
		    grade = "5th Soldier";
		    break;
		case 6:
		    grade = "4th Soldier";
		    break;
		case 7:
		    grade = "3rd Soldier";
		    break;
		case 8:
		    grade = "2nd Soldier";
		    break;
		case 9:
		    grade = "1st Soldier";
		    break;
		case 10:
		    grade = "1st Officer";
		    break;
		case 11:
		    grade = "2nd Officer";
		    break;
		case 12:
		    grade = "3rd Officer";
		    break;
		case 13:
		    grade = "4th Officer";
		    break;
		case 14:
		    grade = "5th Officer";
		    break;
		case 15:
		    grade = "General";
		    break;
		case 16:
		    grade = "Major";
		    break;
		case 17:
		    grade = "Commander";
		    break;
		case 18:
		    grade = "General Commander";
		    break;
		}
		int point = pc.getAbysspoint();
		pc.sendPackets(new S_SystemMessage(
			"[" + pc.getName() + "] Abyss Point " + point + "points" + " Class : " + grade + " is."));
		break;
	    default:
		pc.sendPackets(new S_SystemMessage("Such a command " + cmd + " does not exist. "));
		break;

	    /*
	     * } else if (cmd.equalsIgnoreCase("Friendship")) { describe(pc);
	     */
	    /*
	     * } else if (cmd.equalsIgnoreCase("Abyss Point")) {
	     */
	    }
	} catch (Exception e) {
	    _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
	}
    }

    private void execute(L1PcInstance pc, String param) {
	// TODO Auto-generated method stub

    }

    private void showHelp(L1PcInstance pc) {
	pc.sendPackets(new S_SystemMessage("\\aH===========< User Commands >==========="));
	pc.sendPackets(new S_SystemMessage("\\aA    .Info. Application is fixed. Coordinate restore. Clan party "));
	pc.sendPackets(new S_SystemMessage("\\aA    .Change password secruity settings. Deslanking. Keel Ranking "));
	pc.sendPackets(new S_SystemMessage("\\aA    .Change name unattended registration. Deserted Shop Lights. Age. "));
	pc.sendPackets(new S_SystemMessage("\\aA    .Hyormak. Drops (comments). Arrange Dolls Information. "));
	pc.sendPackets(new S_SystemMessage("\\aH=========< Have a Good Time >================"));
    }

    private void Ment(L1PcInstance pc, String param) {
	if (param.equalsIgnoreCase("off")) {
	    pc.sendPackets(new S_ChatPacket(pc, "Item acquisition -  OFF  - "));
	    pc.RootMent = false;
	} else if (param.equalsIgnoreCase("on")) {
	    pc.sendPackets(new S_ChatPacket(pc, "Item acquisition -  ON  - "));
	    pc.RootMent = true;
	} else {
	    pc.sendPackets(new S_ChatPacket(pc, ".Drop comment [on/off] input (item acquisition comment setting)"));
	}
    }

    private void autoclanjoin(L1PcInstance pc) {
	try {
	    // Failure condition

	    if (pc.getClanRank() != 10) {
		pc.sendPackets(new S_ServerMessage(92, pc.getName())); // \f1%0There is no Prince or Princess.
		return;
	    }
	    if (pc.isFishing()) {
		pc.sendPackets(new S_SystemMessage("When fishing, the action is limited."));
		return;
	    }
	    if (pc.getClanid() == 0 || pc.getClanid() == 1) {// New blood name
		pc.sendPackets(new S_SystemMessage("There is no clan formation status."));
		return;
	    }
	    if (pc.isPrivateShop()) {
		pc.sendPackets(new S_SystemMessage("You can not use it while you are in private shop."));
		return;
	    }
	    if (pc.isPinkName() || pc.isParalyzed() || pc.isSleeped()) {
		pc.sendPackets(new S_SystemMessage("You can not use it while you are paralysis."));
		return;
	    }
	    if (pc.isDead()) {
		pc.sendPackets(new S_SystemMessage("Unable to run in dead condition."));
		return;
	    }

	    // Failure condition
	    // Guillain Ryokan
	    if (pc.getX() >= 33426 && pc.getX() <= 33435 && pc.getY() >= 32795 && pc.getY() <= 32802
		    && pc.getMapId() == 4) {
		for (L1PcInstance target : L1World.getInstance().getAllPlayers3()) {
		    if (target.getId() != pc.getId()
			    && target.getAccountName().toLowerCase().equals(pc.getAccountName().toLowerCase())
			    && target.isAutoClanjoin()) {
			pc.sendPackets(new S_ChatPacket(pc, "Your auxiliary character is already in the unattended registration state."));
			return;
		    }
		}
		pc.setAutoClanjoin(true);
		L1PolyMorph.undoPolyAutoClanjoin(pc);
		LinAllManager.getInstance().LogLogOutAppend(pc.getName(), pc.getNetConnection().getHostname());
		GameClient client = pc.getNetConnection();
		pc.setNetConnection(null);
		try {
		    pc.save();
		    pc.saveInventory();
		} catch (Exception e) {
		}
		client.setActiveChar(null);
		client.setLoginAvailable();
		client.CharReStart(true);
		client.sendPacket(new S_Unknown2(1)); // Changing the structure for a lease button //
		// Episode U
	    } else {
		pc.sendPackets(new S_ChatPacket(pc, "It can only be used in the space in front of Guillain Village Innkeeper."));
	    }
	} catch (Exception e) {
	    System.out.println(pc.getName() + "Unattended Registration processing error");
	}
    }

    public void execute(L1PcInstance pc, String cmdName, String arg) {
	if (arg.equalsIgnoreCase("on")) {
	    pc.setSkillEffect(L1SkillId.GMSTATUS_HPBAR, 0);
	} else if (arg.equalsIgnoreCase("off")) {
	    pc.removeSkillEffect(L1SkillId.GMSTATUS_HPBAR);

	    for (L1Object obj : pc.getKnownObjects()) {
		if (isHpBarTarget(obj)) {
		    pc.sendPackets(new S_HPMeter(obj.getId(), 0xFF, 0xff));
		}
	    }
	} else {
	    pc.sendPackets(new S_SystemMessage(cmdName + "Please enter [On / Off]."));
	}
    }

    private void phone(L1PcInstance pc, String param) {
	try {
	    long curtime = System.currentTimeMillis() / 1000;
	    if (pc.getQuizTime() + 10 > curtime) {
		long sec = (pc.getQuizTime() + 10) - curtime;
		pc.sendPackets(new S_SystemMessage(sec + "Can be used after a second."));
		return;
	    }
	    StringTokenizer tok = new StringTokenizer(param);
	    String phone = tok.nextToken();
	    Account account = Account.load(pc.getAccountName());
	    if (param.length() < 10) {
		pc.sendPackets(new S_ChatPacket(pc, "There is no number. Enter it again.", 1));
		return;
	    }
	    if (param.length() > 11) {
		pc.sendPackets(new S_ChatPacket(pc, "Incorrect number. Enter again。"));
		return;
	    }
	    if (isDisitAlpha(phone) == false) {
		pc.sendPackets(new S_ChatPacket("Please enter only numbers."));
		return;
	    }
	    if (account.getphone() != null) {
		pc.sendPackets(new S_ChatPacket(pc, "You have already set up a phone number."));
		pc.sendPackets(new S_ChatPacket(pc, "Please send us a letter to Metis when changing the number."));
		return;
	    }
	    account.setphone(phone);
	    Account.updatePhone(account);
	    pc.sendPackets(new S_ChatPacket(pc, " " + phone + "Setup complete. Characters are shipped during initialization."));
	    pc.sendPackets(new S_PacketBox(pc, S_PacketBox.ICON_SECURITY_SERVICES));
	    pc.sendPackets(new S_ChatPacket(pc, "Security Baff (AC) is leased and applied"));
	    pc.setQuizTime(curtime);
	} catch (Exception e) {
	    pc.sendPackets(new S_ChatPacket(pc, "Input in the form of fixed application contact (transfer characters only when initializing)"));
	}
    }

    private void initStatus(L1PcInstance pc) {
	L1SkillUse l1skilluse = new L1SkillUse();
	l1skilluse.handleCommands(pc, L1SkillId.CANCELLATION, pc.getId(), pc.getX(), pc.getY(), null, 0,
		L1SkillUse.TYPE_LOGIN);

	if (pc.getWeapon() != null) {
	    pc.getInventory().setEquipped(pc.getWeapon(), false, false, false, false);
	}

	pc.sendPackets(new S_CharVisualUpdate(pc));
	pc.sendPackets(new S_OwnCharStatus2(pc));

	for (L1ItemInstance armor : pc.getInventory().getItems()) {
	    for (int type = 0; type <= 12; type++) {
		if (armor != null) {
		    pc.getInventory().setEquipped(armor, false, false, false, false);
		}
	    }
	}
	pc.setReturnStat(pc.getExp());
	pc.sendPackets(new S_SPMR(pc));
	pc.sendPackets(new S_OwnCharAttrDef(pc));
	pc.sendPackets(new S_OwnCharStatus2(pc));
	pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.START));
	try {
	    pc.save();
	} catch (Exception e) {
	    System.out.println("Status initialization command error");
	}
    }

    private void statusInitialization(L1PcInstance pc) {
	try {
	    long curtime = System.currentTimeMillis() / 1000;
	    if (pc.getQuizTime() + 10 > curtime) {
		pc.sendPackets(new S_SystemMessage("Requires a delay of 10 seconds."));
		return;
	    }
	    if (!pc.getMap().isSafetyZone(pc.getLocation())) {
		pc.sendPackets(new S_ChatPacket(pc, "It can be used only in safe areas."));
		return;
	    }
	    if (pc.getInventory().checkItem(200000, 1)) {
		if (pc.getLevel() != pc.getHighLevel()) {
		    pc.sendPackets(new S_SystemMessage("The character that the level is down. Please use after leveling up."));
		    return;
		}
		if (pc.getLevel() > 54) {
		    pc.getInventory().consumeItem(200000, 1);
		    new L1Teleport().teleport(pc, 32723 + _random.nextInt(10), 32851 + _random.nextInt(10),
			    (short) 5166, 5, true);
		    initStatus(pc);
		} else {
		    pc.sendPackets(new S_SystemMessage("Status initialization is only possible for level 55 or higher."));
		}
	    } else {
		pc.sendPackets(new S_SystemMessage("There is no recollection candle."));
		return;
	    }

	    pc.setQuizTime(curtime);
	} catch (Exception e) {
	}
    }

    private void check(L1PcInstance pc) {
	try {
	    long curtime = System.currentTimeMillis() / 1000;
	    if (pc.getQuizTime() + 10 > curtime) {
		pc.sendPackets(new S_SystemMessage("Requires a delay of 10 seconds."));
		return;
	    }
	    int hpr = pc.getHpr() + pc.getInventory().hpRegenPerTick();
	    int mpr = pc.getMpr() + pc.getInventory().mpRegenPerTick();

	    pc.sendPackets(new S_SystemMessage("===================( My Information )===================="));
	    pc.sendPackets(new S_SystemMessage("\\aD(HPR: " + hpr + ')' + "(MPR: " + mpr + ')' + "(PK Count: "
		    + pc.get_PKcount() + ')' + "(Elixir: " + pc.getElixirStats() + "本)"));
	    pc.sendPackets(new S_SystemMessage("===================================================="));
	    pc.setQuizTime(curtime);
	} catch (Exception e) {
	}
    }

    private void maphack(L1PcInstance pc, String param) {
	try {
	    StringTokenizer st = new StringTokenizer(param);
	    String on = st.nextToken();
	    if (on.equalsIgnoreCase("on")) {
		pc.sendPackets(new S_Ability(3, true));
		pc.sendPackets(new S_SystemMessage("\\aACommand: Light \\aG[start]\\aA Did."));
	    } else if (on.equals("off")) {
		pc.sendPackets(new S_Ability(3, false));
		pc.sendPackets(new S_SystemMessage("\\aACommand: Light \\aG[End]\\aA Did."));
	    }
	} catch (Exception e) {
	    pc.sendPackets(new S_SystemMessage("\\aACommand:. Light \\aG[On, off]"));
	}
    }

    private void location(L1PcInstance pc) {
	try {
	    long curtime = System.currentTimeMillis() / 1000;
	    if (pc.getQuizTime2() + 20 > curtime) {
		long time = (pc.getQuizTime2() + 20) - curtime;
		pc.sendPackets(new S_ChatPacket(pc, time + "Can be used after a second."));
		return;
	    }
	    Connection connection = null;
	    connection = L1DatabaseFactory.getInstance().getConnection();
	    PreparedStatement preparedstatement = connection.prepareStatement(
		    "UPDATE characters SET LocX=33432,LocY=32807,MapID=4 WHERE account_name=? and MapID not in (5001,99,997,5166,39,34,701,2000)"); // gm's room, prison, battle zone waiting room
	    // Exclude<
	    preparedstatement.setString(1, pc.getAccountName());
	    preparedstatement.execute();
	    preparedstatement.close();
	    connection.close();
	    pc.sendPackets(new S_SystemMessage("The coordinates of all the characters in the account have been moved to Gillan village"));

	    pc.setQuizTime(curtime);
	} catch (Exception e) {
	}
    }

    private void tell(L1PcInstance pc) {

	long curtime = System.currentTimeMillis() / 1000;
	if (pc.getQuizTime2() + 20 > curtime) {
	    long time = (pc.getQuizTime2() + 20) - curtime;
	    pc.sendPackets(new S_ChatPacket(pc, time + "Can be used after a second."));
	    return;
	}
	try {
	    if (pc.getMapId() == 781) {
		if (pc.getLocation().getX() <= 32998 && pc.getLocation().getX() >= 32988
			&& pc.getLocation().getY() <= 32758 && pc.getLocation().getY() >= 32736) {
		    pc.sendPackets(new S_SystemMessage("It is a place you can not use."));
		    return;
		}
	    }
	    if (pc.isPinkName() || pc.isDead() || pc.isParalyzed() || pc.isSleeped() || pc.getMapId() == 800
		    || pc.getMapId() == 5302 || pc.getMapId() == 5153 || pc.getMapId() == 5490) {
		pc.sendPackets(new S_SystemMessage("It can not be used."));
		return;
	    }
	    new L1Teleport().teleport(pc, pc.getX(), pc.getY(), pc.getMapId(), pc.getHeading(), false);
	    pc.update_lastLocalTellTime();
	    pc.setQuizTime2(curtime);
	} catch (Exception exception35) {
	}
    }

    private void Mark1(L1PcInstance pc, String param) {
	long curtime = System.currentTimeMillis() / 1000;
	if (pc.getQuizTime() + 30 > curtime) {
	    long time = (pc.getQuizTime() + 30) - curtime;
	    pc.sendPackets(new S_ChatPacket(pc, time + "Can be used after a second."));
	    return;
	}
	if (pc.isDead()) {
	    pc.sendPackets(new S_SystemMessage("cannot be used in dead condition."));
	    return;
	}
	int i = 1;
	if (pc.watchCrest) {
	    i = 3;
	    pc.watchCrest = false;
	} else
	    pc.watchCrest = true;
	for (L1Clan clan : L1World.getInstance().getAllClans()) {
	    if (clan != null) {
		pc.sendPackets(new S_War(i, pc.getClanname(), clan.getClanName()));
	    }
	}
	pc.setQuizTime(curtime);

    }

    public void BloodParty(L1PcInstance pc) {
	if (pc.isDead()) {
	    pc.sendPackets(new S_SystemMessage("cannot be used in dead condition."));
	    return;
	}
	int ClanId = pc.getClanid();
	if (ClanId != 0 && pc.getClanRank() == L1Clan.MONARCH || pc.getClanRank() == L1Clan.GUARDIAN
		|| pc.getClanRank() == L1Clan.SUB_MONARCH) {
	    for (L1PcInstance SearchBlood : L1World.getInstance().getAllPlayers()) {
		if (SearchBlood.getClanid() != ClanId || SearchBlood.isPrivateShop() || SearchBlood.isAutoClanjoin()
			|| SearchBlood.isInParty()) { // If the clan is the same, [X]
		    // If you are already at a party [X], in the store [X]
		    continue; // Gun escape
		} else if (SearchBlood.getName() != pc.getName()) {
		    pc.setPartyType(1); // Party Type settings
		    SearchBlood.setPartyID(pc.getId()); // Party name setting
		    SearchBlood.sendPackets(new S_Message_YN(954, pc.getName()));
		    pc.sendPackets(new S_ChatPacket(pc, SearchBlood.getName() + "You applied for a party."));
		}
	    }
	} else { // Monarch and guardian knight without clan [X]
	    pc.sendPackets(new S_ChatPacket(pc, "It is possible to use the monarch, the monarch, and the Guardian knight though there is clan."));
	}
    }

    private void age(L1PcInstance pc, String cmd) {
	try {
	    StringTokenizer tok = new StringTokenizer(cmd);
	    String AGE = tok.nextToken();
	    int AGEint = Integer.parseInt(AGE);
	    if (AGEint > 59 || AGEint < 14) {
		pc.sendPackets(new S_ChatPacket(pc, "Set to their actual age."));
		return;
	    }
	    pc.setAge(AGEint);
	    pc.save();
	    pc.sendPackets(new S_ChatPacket(pc, "Command: Your age is  [" + AGEint + "] is set."));
	} catch (Exception e) {
	    pc.sendPackets(new S_ChatPacket(pc, ". Input in the form of age numbers (clan chat time)"));
	}
    }

    private void Hunt(L1PcInstance pc, String cmd) {
	try {
	    StringTokenizer st = new StringTokenizer(cmd);
	    String char_name = st.nextToken();
	    int price = Integer.parseInt(st.nextToken());
	    String story = st.nextToken();

	    L1PcInstance target = null;
	    target = L1World.getInstance().getPlayer(char_name);
	    if (target != null) {
		if (target.isGm()) {
		    return;
		}
		if (target.getHuntCount() == 1) {
		    pc.sendPackets(new S_SystemMessage("It's already been arranged."));
		    return;
		}
		if (price != Config.STAGE_1 && price != Config.STAGE_2 && price != Config.STAGE_3) {
		    pc.sendPackets(new S_SystemMessage(
			    "Unit amount is, " + Config.STAGE_1 + "/" + Config.STAGE_2 + "/" + Config.STAGE_3 + " adena"));
		    pc.sendPackets(new S_SystemMessage(
			    "Ex.) " + Config.STAGE_1 + ", " + Config.STAGE_2 + ", " + Config.STAGE_3 + "Until"));
		    return;
		}
		if (price > Config.STAGE_3) {
		    pc.sendPackets(new S_SystemMessage("The maximum amount is" + Config.STAGE_3 + "adena"));
		    return;
		}
		if (!(pc.getInventory().checkItem(40308, price))) {
		    pc.sendPackets(new S_SystemMessage("Missing Adena"));
		    return;
		}
		if (story.length() > 20) {
		    pc.sendPackets(new S_SystemMessage("Reason short, Please enter 20 characters or more."));
		    return;
		}
		if (target.getHuntPrice() > Config.STAGE_3) {
		    pc.sendPackets(new S_SystemMessage("Maximum order amount" + Config.STAGE_3 + "is only."));
		    return;
		}
		target.setHuntCount(1);
		target.setHuntPrice(target.getHuntPrice() + price);
		target.setReasonToHunt(story);
		target.save();
		L1World.getInstance().broadcastServerMessage("\\aD[" + target.getName() + "]Took a prize in the neck.");
		L1World.getInstance()
			.broadcastPacketToAll(new S_SystemMessage("\\aD[ Mentor ]:  " + target.getName() + "  ]"));
		L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aD[ Reason ]: " + story + "  "));
		pc.getInventory().consumeItem(40308, price);
		huntoption(target);
	    } else {
		pc.sendPackets(new S_SystemMessage("Not connected."));
	    }
	} catch (Exception e) {
	    pc.sendPackets(new S_SystemMessage(". Arrange[character name] [amount] [reason]"));
	    pc.sendPackets(new S_SystemMessage("====== Additional batting range ======"));
	    pc.sendPackets(new S_SystemMessage("====== " + Config.STAGE_1 + "million1 ======"));
	    pc.sendPackets(new S_SystemMessage("====== " + Config.STAGE_2 + "million2 ======"));
	    pc.sendPackets(new S_SystemMessage("====== " + Config.STAGE_3 + "million3 ======"));
	}
    }

    private void huntoption(L1PcInstance pc) { // Show this map effect
	if (pc.getHuntCount() != 0) {
	    if (pc.isWizard() || pc.isBlackwizard()) {
		if (pc.getHuntPrice() == Config.STAGE_1) {
		    pc.addSp(1);
		    pc.sendPackets(new S_SPMR(pc));
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		} else if (pc.getHuntPrice() == Config.STAGE_2) {
		    pc.addSp(2);
		    pc.sendPackets(new S_SPMR(pc));
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		} else if (pc.getHuntPrice() == Config.STAGE_3) {
		    pc.addSp(3);
		    pc.sendPackets(new S_SPMR(pc));
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		}
	    } else if (pc.isCrown() || pc.isKnight() || pc.isDarkelf() || pc.isDragonknight() || pc.isWarrior()) {
		if (pc.getHuntPrice() == Config.STAGE_1) {
		    pc.addDmgup(1);
		    pc.addBowDmgup(1);
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		} else if (pc.getHuntPrice() == Config.STAGE_2) {
		    pc.addDmgup(2);
		    pc.addBowDmgup(2);
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		} else if (pc.getHuntPrice() == Config.STAGE_3) {
		    pc.addDmgup(3);
		    pc.addBowDmgup(3);
		    pc.sendPackets(new S_OwnCharAttrDef(pc));
		    pc.sendPackets(new S_OwnCharStatus2(pc));
		    pc.sendPackets(new S_OwnCharStatus(pc));
		}
	    }
	}
    }

    private static boolean isDisitAlpha(String str) {
	boolean check = true;
	for (int i = 0; i < str.length(); i++) {
	    if (!Character.isDigit(str.charAt(i)) // If there are no numbers
		    && !Character.isUpperCase(str.charAt(i)) // If there is no capital letter
		    && !Character.isLowerCase(str.charAt(i))) { // If there is no lowercase letter
		check = false;
		break;
	    }
	}
	return check;
    }

    private boolean isValidQuiz(L1PcInstance pc, String quiz) {
	java.sql.Connection con = null;
	PreparedStatement statement = null;
	ResultSet rs = null;
	boolean result = false;

	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    statement = con.prepareStatement("select quiz from accounts where login='" + pc.getAccountName() + "'");
	    rs = statement.executeQuery();

	    String oldQuiz = "";
	    if (rs.next()) {
		oldQuiz = rs.getString(1);
	    }

	    if (oldQuiz == null || oldQuiz.equalsIgnoreCase(quiz)) {
		result = true;
	    }
	} catch (Exception e) {
	} finally {
	    SQLUtil.close(rs);
	    SQLUtil.close(statement);
	    SQLUtil.close(con);
	}

	return result;
    }

    private void changequiz(L1PcInstance pc, String param) {
	boolean firstQuiz = false;
	try {
	    StringTokenizer tok = new StringTokenizer(param);
	    String oldquiz = "";

	    if (isValidQuiz(pc, oldquiz)) {
		firstQuiz = true;
	    } else {
		oldquiz = tok.nextToken();
	    }
	    String newquiz = tok.nextToken();

	    if (newquiz.length() < 4) {
		pc.sendPackets(new S_ChatPacket(pc, "Enter in English or numbers between 4 characters and 12 characters."));
		return;
	    }
	    if (newquiz.length() > 12) {
		pc.sendPackets(new S_ChatPacket(pc, "Enter in English or numbers between 4 characters and 12 characters."));
		return;
	    }

	    if (isDisitAlpha(newquiz) == false) {
		pc.sendPackets(new S_ChatPacket(pc, "Please enter only numbers and English."));
		return;
	    }
	    chkquiz(pc, oldquiz, newquiz);
	} catch (Exception e) {
	    if (firstQuiz) {
		pc.sendPackets(new S_ChatPacket(pc, "Enter your security settings in the required security password format."));
	    } else {
		pc.sendPackets(new S_ChatPacket(pc, "Your account security has already been setup."));
	    }
	}
    }

    private void chkquiz(L1PcInstance pc, String oldQuiz, String newQuiz) {
	java.sql.Connection con = null;
	PreparedStatement pstm = null;

	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    String sqlstr = "UPDATE accounts SET quiz = ? WHERE login=?";
	    pstm = con.prepareStatement(sqlstr);
	    pstm.setString(1, newQuiz);
	    pstm.setString(2, pc.getAccountName());
	    pstm.execute();
	    pc.sendPackets(new S_SystemMessage("\\aDSecurity setting completed successfully."));
	    pc.sendPackets(new S_SystemMessage("\\aDSecurity password：" + newQuiz + "(You can not change the password for lost account)"));
	    pc.setNeedQuiz(false);
	    pc.update_lastQuizChangeTime();
	} catch (Exception e) {
	} finally {
	    SQLUtil.close(pstm);
	    SQLUtil.close(con);
	}
    }

    private void validateQuiz(L1PcInstance pc, String param) {
	try {
	    StringTokenizer tok = new StringTokenizer(param);
	    if (isValidQuiz(pc, "")) {
		pc.sendPackets(new S_ChatPacket(pc, "First, you need a security setting. command [Security Settings] "));
		return;
	    }
	    String quiz = tok.nextToken();

	    if (!isValidQuiz(pc, quiz)) {
		Accountsquiz(pc, quiz);
		return;
	    }
	    pc.setQuizValidated();
	    pc.sendPackets(new S_ChatPacket(pc, "Security has been turned off for a while. You can change your password for a while.", 1));
	} catch (Exception e) {
	    pc.sendPackets(new S_ChatPacket(pc, "Security unlock Enter the password in the form of a security set."));
	}
    }

    private void Accountsquiz(L1PcInstance pc, String quiz) {
	java.sql.Connection con = null;
	PreparedStatement statement = null;
	ResultSet rs = null;
	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    statement = con.prepareStatement("select quiz from accounts where login='" + pc.getAccountName() + "'");
	    rs = statement.executeQuery();
	    String oldQuiz = "";
	    if (rs.next()) {
		oldQuiz = rs.getString(1);
		pc.sendPackets(new S_ChatPacket(pc, "The password does not match the security settings. *Tips：" + oldQuiz.length() + " character."));
	    }
	} catch (Exception e) {
	} finally {
	    SQLUtil.close(rs);
	    SQLUtil.close(statement);
	    SQLUtil.close(con);
	}
    }

    private void changepassword(L1PcInstance pc, String param) {
	// boolean firstQuiz = false;
	try {
	    if (pc.get_lastPasswordChangeTime() + 10 * 60 * 1000 > System.currentTimeMillis()) {
		pc.sendPackets(new S_ChatPacket(pc, "Installed from the decision to change the password, it is not past 10 minutes. Please return back and change."));
		return;
	    }
	    StringTokenizer tok = new StringTokenizer(param);
	    String newpasswd = tok.nextToken();
	    if (isValidQuiz(pc, "")) {
		pc.sendPackets(new S_ChatPacket(pc, "After security settings, you can change your password. command [Security Settings]"));
		return;
	    }
	    if (!pc.isQuizValidated()) {
		pc.sendPackets(new S_ChatPacket(pc, "You can change your password after you remove the security. command [security release]"));
		return;
	    }
	    if (newpasswd.length() < 6) {
		pc.sendPackets(new S_ChatPacket(pc, "Enter in english or numbers between 6 - 16 characters."));
		return;
	    }
	    if (newpasswd.length() > 16) {
		pc.sendPackets(new S_ChatPacket(pc, "Enter in english or numbers between 6 - 16 characters."));
		return;
	    }
	    if (isDisitAlpha(newpasswd) == false) {
		pc.sendPackets(new S_ChatPacket(pc, "Please enter only English and numbers."));
		return;
	    }
	    to_Change_Passwd(pc, newpasswd);

	} catch (Exception e) {
	    pc.sendPackets(new S_ChatPacket(pc, "Change Password Password< - Please enter in the format."));
	}
    }

    private void to_Change_Passwd(L1PcInstance pc, String passwd) {
	try {
	    String login = null;
	    String password = null;
	    java.sql.Connection con = null;
	    con = L1DatabaseFactory.getInstance().getConnection();
	    PreparedStatement statement = null;
	    PreparedStatement pstm = null;

	    password = passwd;

	    statement = con.prepareStatement(
		    "select account_name from characters where char_name Like '" + pc.getName() + "'");
	    ResultSet rs = statement.executeQuery();

	    while (rs.next()) {
		login = rs.getString(1);
		pstm = con.prepareStatement("UPDATE accounts SET password=? WHERE login Like '" + login + "'");
		pstm.setString(1, password);
		pstm.execute();

		pc.sendPackets(new S_ChatPacket(pc, "Your account password has been changed to（" + passwd + "."));
	    }
	    rs.close();
	    pstm.close();
	    statement.close();
	    con.close();
	} catch (Exception e) {
	}
    }

    // Returns whether the password is correct
    public static boolean isPasswordTrue(String Password, String oldPassword) {
	String _rtnPwd = null;
	Connection con = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	boolean result = false;
	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    pstm = con.prepareStatement("SELECT password(?) as pwd");

	    pstm.setString(1, oldPassword);
	    rs = pstm.executeQuery();
	    if (rs.next()) {
		_rtnPwd = rs.getString("pwd");
	    }
	    if (_rtnPwd.equals(Password)) { // If they are the same
		result = true;
	    }
	} catch (Exception e) {
	    _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
	} finally {
	    SQLUtil.close(rs);
	    SQLUtil.close(pstm);
	    SQLUtil.close(con);
	}
	return result;
    }

    private void POPall(L1PcInstance pc) {
	pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "LINALL1"));
    }

    private void changename(L1PcInstance pc, String name) {
	if (BadNamesList.getInstance().isBadName(name)) {
	    pc.sendPackets(new S_SystemMessage("It is a character name forbidden."));
	    return;
	}
	if (CharacterTable.doesCharNameExist(name)) { // character
	    pc.sendPackets(new S_SystemMessage("The same name exists."));
	    return;
	}
	if (pc.getClanid() != 0) {
	    pc.sendPackets(new S_SystemMessage("You can change the clan after you leave it for a while."));
	    return;
	}
	if (pc.isCrown()) {
	    pc.sendPackets(new S_SystemMessage("The royal can change only after consultation with the gm."));
	    return;
	}
	if (pc.hasSkillEffect(1005) || pc.hasSkillEffect(2005)) {
	    pc.sendPackets(new S_SystemMessage("I can not change to the state of gold mine."));
	    return;
	}
	try {
	    if (pc.getLevel() >= 60) {
		for (int i = 0; i < name.length(); i++) {
		    if (!Character.isLetterOrDigit(name.charAt(i))) {
			pc.sendPackets(new S_SystemMessage("Character name is incorrect."));
			return;
		    }
		}
		int numOfNameBytes = 0;
		numOfNameBytes = name.getBytes("MS932").length;
		if (numOfNameBytes == 0) {
		    pc.sendPackets(new S_SystemMessage("Rename change character name < - Type name and enter"));
		    return;
		}
		if (numOfNameBytes < 2 || numOfNameBytes > 12) {
		    pc.sendPackets(new S_SystemMessage("Please enter between 2 and 6 characters."));
		    return;
		}

		if (BadNamesList.getInstance().isBadName(name)) {
		    pc.sendPackets(new S_SystemMessage("Character name forbidden."));
		    return;
		}
		if (RobotAIThread.doesCharNameExist(name)) { // robot
		    pc.sendPackets(new S_SystemMessage("The same name exists."));
		    return;
		}

		if (pc.getInventory().checkItem(408990, 1)) { // Inventory item check
		    Connection con = null;
		    PreparedStatement pstm = null;
		    try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE characters SET char_name =? WHERE char_name = ?");
			pstm.setString(1, name); // Change
			pstm.setString(2, pc.getName());
			pstm.execute();
		    } catch (SQLException e) {
		    } finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		    }

		    pc.save(); // Save
		    /****** Create a character name change in the file here part *******/

		    /****** LogDB Create a folder named in advance Please *******/
		    Calendar rightNow = Calendar.getInstance();
		    int year = rightNow.get(Calendar.YEAR);
		    int month = rightNow.get(Calendar.MONTH) + 1;
		    int date = rightNow.get(Calendar.DATE);
		    int hour = rightNow.get(Calendar.HOUR);
		    int min = rightNow.get(Calendar.MINUTE);
		    String stryyyy = "";
		    String strmmmm = "";
		    String strDate = "";
		    String strhour = "";
		    String strmin = "";
		    stryyyy = Integer.toString(year);
		    strmmmm = Integer.toString(month);
		    strDate = Integer.toString(date);
		    strhour = Integer.toString(hour);
		    strmin = Integer.toString(min);
		    String str = "";
		    str = new String("[" + stryyyy + "-" + strmmmm + "-" + strDate + " " + strhour + ":" + strmin
			    + "]  " + pc.getName() + "  --->  " + name);
		    StringBuffer FileName = new StringBuffer("LogDB/ChangeCharacterName.txt");
		    PrintWriter out = null;
		    try {
			out = new PrintWriter(new FileWriter(FileName.toString(), true));
			out.println(str);
			out.close();
		    } catch (IOException e) {
			
			e.printStackTrace();
		    }
		    str = "";// Initialization
		    pc.getInventory().consumeItem(408990, 1); // Delete purchase order
		    pc.sendPackets(new S_SystemMessage("When you reconnect, it will be changed to a new name."));
		    buddys(pc); // Delete friend
		    deleteMail(pc); // Delete mail
		    Thread.sleep(500);
		    pc.sendPackets(new S_Disconnect());

		} else {
		    pc.sendPackets(new S_SystemMessage("Name change shortage."));
		}
	    } else {
		pc.sendPackets(new S_SystemMessage("Only 60 levels or more are possible."));
	    }

	} catch (Exception e) {
	    pc.sendPackets(new S_SystemMessage("Rename Change Please enter by character name."));
	}
    }

    /********* Clear changed user name from Divi friends list ************/

    private void buddys(L1PcInstance pc) {
	Connection con = null;
	PreparedStatement pstm = null;
	String aaa = pc.getName();
	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    pstm = con.prepareStatement("DELETE FROM character_buddys WHERE buddy_name=?");

	    pstm.setString(1, aaa);
	    pstm.execute();
	} catch (SQLException e) {
	} finally {
	    SQLUtil.close(pstm);
	    SQLUtil.close(con);
	}
    }

    private void deleteMail(L1PcInstance pc) {
	Connection con = null;
	PreparedStatement pstm = null;

	String aaa = pc.getName();

	try {
	    con = L1DatabaseFactory.getInstance().getConnection();
	    pstm = con.prepareStatement("DELETE FROM letter WHERE receiver=?");
	    pstm.setString(1, aaa);
	    pstm.execute();
	    // System.out.println("....["+ aaa +"].....");
	} catch (SQLException e) {
	} finally {
	    SQLUtil.close(pstm);
	    SQLUtil.close(con);
	}
    }

    private void privateShop1(L1PcInstance pc) {
	try {
	    if (!pc.isPrivateShop()) {
		pc.sendPackets(new S_ChatPacket(pc, "Notice: It is possible to use it in a personal shop state."));
		return;
	    }
	    // manager.LogServerAppend("終了", pc, pc.getNetConnection().getIp(),
	    // -1);
	    LinAllManager.getInstance().LogLogOutAppend(pc.getName(), pc.getNetConnection().getHostname());
	    GameClient client = pc.getNetConnection();
	    pc.setNetConnection(null);
	    // pc.stopMpRegeneration();
	    try {
		pc.save();
		pc.saveInventory();
	    } catch (Exception e) {
	    }
	    client.setActiveChar(null);
	    client.setLoginAvailable();
	    client.CharReStart(true);
	    client.sendPacket(new S_Unknown2(1));
	} catch (Exception e) {
	}
    }

    private void privateShop(L1PcInstance pc) {
	try {
	    if (!pc.isPrivateShop()) {
		pc.sendPackets(new S_ChatPacket(pc, "Notice: It is possible to use it in a personal shop state."));
		return;
	    }
	    for (L1PcInstance target : L1World.getInstance().getAllPlayers3()) {
		if (target.getId() != pc.getId()
			&& target.getAccountName().toLowerCase().equals(pc.getAccountName().toLowerCase())
			&& target.isPrivateShop()) {
		    pc.sendPackets(new S_ChatPacket(pc, "Warning: Your auxiliary character is already in an unattended shop."));
		    return;
		}
	    }
	    // manager.LogServerAppend("終了", pc, pc.getNetConnection().getIp(),
	    // -1);
	    LinAllManager.getInstance().LogLogOutAppend(pc.getName(), pc.getNetConnection().getHostname());
	    GameClient client = pc.getNetConnection();
	    pc.setNetConnection(null);
	    // pc.stopMpRegeneration();
	    try {
		pc.save();
		pc.saveInventory();
	    } catch (Exception e) {
	    }
	    client.setActiveChar(null);
	    client.setLoginAvailable();
	    client.CharReStart(true);
	    client.sendPacket(new S_Unknown2(1)); // Changing the structure for a lease button //
	    // Episode U

	} catch (Exception e) {
	}
    }

    public static boolean isHpBarTarget(L1Object obj) {
	if (obj instanceof L1MonsterInstance) {
	    return true;
	}
	if (obj instanceof L1PcInstance) {
	    return true;
	}
	if (obj instanceof L1SummonInstance) {
	    return true;
	}
	if (obj instanceof L1PetInstance) {
	    return true;
	}
	return false;
    }

    private boolean executeDatabaseCommand(L1PcInstance pc, String name, String arg) {
	try {
	    L1Command command = L1Commands.get(name);
	    if (command == null) {
		return false;
	    }
	    if (pc.getAccessLevel() < command.getLevel()) {
		pc.sendPackets(new S_ServerMessage(74, "Command" + name));
		// \f1%0Can not be a gift.
		return true;
	    }

	    Class<?> cls = Class.forName(complementClassName(command.getExecutorClassName()));
	    L1CommandExecutor exe = (L1CommandExecutor) cls.getMethod("getInstance").invoke(null);
	    exe.execute(pc, name, arg);
	    // manager.LogCommandAppend(pc.getName(), name, arg);
	    LinAllManager.getInstance().GmAppend(pc.getName(), name, arg);
	    /** Save File Log**/
	    LoggerInstance.getInstance().addCommand(pc.getName() + ": " + name + " " + arg);
	    return true;
	} catch (Exception e) {
	    _log.log(Level.SEVERE, "error gm command", e);
	}
	return false;
    }

    private String complementClassName(String className) {
	if (className.contains(".")) {
	    return className;
	}
	return "l1j.server.server.command.executor." + className;
    }
}
// }