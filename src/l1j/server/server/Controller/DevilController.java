package l1j.server.server.Controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import l1j.server.Config;
import l1j.server.server.datatables.DoorSpawnTable;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DoorInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class DevilController extends Thread {

    private static DevilController _instance;

    private boolean _DevilStart;

    public boolean getDevilStart() {
        return _DevilStart;
    }

    public void setDevilStart(boolean Devil) {
        _DevilStart = Devil;
    }

    private static long sTime = 0;

    public boolean isGmOpen = false; // add to

    private String NowTime = "";

    //Time interval
    private static final int LOOP = Config.GLUDIN_DUNGEON_OPEN_CYCLE;
    ;

    private static final SimpleDateFormat s = new SimpleDateFormat("HH",
            Locale.KOREA);

    private static final SimpleDateFormat ss = new SimpleDateFormat(
            "MM-dd HH:mm", Locale.KOREA);

    public static DevilController getInstance() {
        if (_instance == null) {
            _instance = new DevilController();
        }
        return _instance;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                /** open * */
                if (!isOpen() && !isGmOpen)
                    continue;
                if (L1World.getInstance().getAllPlayers().size() <= 0)
                    continue;

                isGmOpen = false;

                /** Open message * */
                L1SpawnUtil.spawn2(33430, 32802, (short) 4, 200001, 0, 3600 * 1000, 0);
                L1SpawnUtil.spawn2(33430, 32802, (short) 4, 199997, 0, 3600 * 1000, 0);
                //		L1SpawnUtil.spawn2(33430, 32802, (short) 4, 777854, 0, 3600 * 1000, 0);

                L1SpawnUtil.spawn2(33430, 32824, (short) 4, 200001, 0, 3600 * 1000, 0);
                L1SpawnUtil.spawn2(33430, 32824, (short) 4, 199997, 0, 3600 * 1000, 0);
                //	L1SpawnUtil.spawn2(33430, 32824, (short) 4, 777854, 0, 3600 * 1000, 0);

                L1SpawnUtil.spawn2(32693, 32798, (short) 450, 200001, 0, 3600 * 1000, 0);
                L1SpawnUtil.spawn2(32693, 32798, (short) 450, 199997, 0, 3600 * 1000, 0);
                //	L1SpawnUtil.spawn2(32693, 32798, (short) 450, 777854, 0, 3600 * 1000, 0);

                L1SpawnUtil.spawn2(32863, 32839, (short) 530, 45955, 0, 3600 * 1000, 0);
            /*	reset(530, 4058);
                reset(531, 4059);
				reset(531, 4060);
				reset(531, 4061);
				reset(532, 4062);
				reset(533, 4063);
				reset(533, 4064);
				reset(533, 4065);
				reset(534, 4066);*/
                //UserRankingController.isRenewal = true; //Ranking renewal Once in 3 hours
                L1World.getInstance().broadcastPacketToAll(
                        new S_SystemMessage("\\aGThe portal was created on the 4th floor of the old rastabad dungeon."));
                L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f3The portal was created on the 4th floor of the old rastabad dungeon."));
                /** Start the devil king's territory* */
                setDevilStart(true);

                /** Run 1 hour start* */


                Thread.sleep(3600000L); // 3600000L 1 hour 10 minutes

                /** Automatic teleport after 1 hour* */
                TelePort();
                close(); //add to
                Thread.sleep(5000L);
                TelePort2();

                /** End * */
                End();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Bring an open time
     *
     * @return (Strind）Open time（MM-dd HH：mm）
     */
    public String OpenTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(sTime);
        return ss.format(c.getTime());
    }

    /**
     * Confirm that the territory is open
     *
     * @return (boolean) True if it is open true closed false
     */
    private boolean isOpen() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % LOOP) == 0)
            return true;
        return false;
    }

    /**
     * In fact, bring the current time
     *
     * @return (String) Current time（HH：mm）
     */
    private String getTime() {
        return s.format(Calendar.getInstance().getTime());
    }

    /**
     * アデン村にティンギが*
     */
    private void TelePort() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 530:
                case 531:
                case 532:
                case 533:
                case 534:
                case 535:
                case 536:
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 33437, 32799, (short) 4, 4, true);
                    L1World.getInstance().broadcastPacketToAll(
                            new S_SystemMessage("\\aGThe portal has disappeared on the 4th floor of the old rastabad dungeon."));
                    L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f3The portal has disappeared on the 4th floor of the old rastabad dungeon."));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * If the character dies, end it
     **/
    private void close() {
        for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
            if ((pc.getMap().getId() >= 530 && pc.getMap().getId() <= 536) && pc.isDead()) {
                pc.stopHpRegenerationByDoll();
                pc.stopMpRegenerationByDoll();
                pc.sendPackets(new S_Disconnect());
            }
        }
    }

    /**
     * アデン村にティンギが*
     */
    private void TelePort2() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 530:
                case 531:
                case 532:
                case 533:
                case 534:
                case 535:
                case 536:
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 33437, 32799, (short) 4, 4, true);
                    break;
                default:
                    break;
            }
        }
    }

    private void reset(int mapId, int relatedDoor) {
        if (relatedDoor == 0) return;
        L1DoorInstance door = DoorSpawnTable.getInstance().getDoor(relatedDoor);
        if (door != null) {
            door.setDead(false); // Is it mana?
            door.close();
        }
    }

    /**
     * End *
     */
    private void End() {
        L1World.getInstance().broadcastServerMessage("The portal has disappeared on the 4th floor of the old rastabad dungeon.");
        setDevilStart(false);
        reset(530, 4058);
        reset(531, 4059);
        reset(531, 4060);
        reset(531, 4061);
        reset(532, 4062);
        reset(533, 4063);
        reset(533, 4064);
        reset(533, 4065);
        reset(534, 4066);
    }
}
