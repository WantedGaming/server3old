package l1j.server.GameSystem.MiniGame;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import l1j.server.Config;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class HellController extends Thread {

    private static HellController _instance;

    private L1PcInstance[] RaidList;

    private boolean _HellStart;

    public boolean getHellStart() {
        return _HellStart;
    }

    public void setHellStart(boolean Hell) {
        _HellStart = Hell;
    }

    private boolean _RaidJoin;

    public boolean getRaidJoin() {
        return _RaidJoin;
    }

    public void setRaidJoin(boolean Raid) {
        _RaidJoin = Raid;
    }

    private static long sTime = 0;
    private String NowTime = "";
    public boolean isGmOpen3;
    private static final int HELLTIME = Config.HELL_TIME;
    public final ArrayList<L1PcInstance> _Members = new ArrayList<L1PcInstance>();
    private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);
    private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);


    public static HellController getInstance() {
        if (_instance == null) {
            _instance = new HellController();
        }
        return _instance;
    }

    private HellController() {
        System.out.println("■Hell Hunting Data..........................■ Load Successful.");
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                /** オープン **/
                if (!isOpen6() && !isGmOpen3)
                    continue;
                if (L1World.getInstance().getAllPlayers().size() <= 0)
                    continue;

                isGmOpen3 = false;

                /** Open message **/
                L1World.getInstance().broadcastServerMessage("The lottery for admission to Hell has started. Please join us.");
                L1World.getInstance().broadcastServerMessage("Type > [.joinHell] in the chat window.");
                L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "Hell has open it's door.  You will be able to enter in 1 hour."));

                /** Start the Territory of Hell**/
                setHellStart(true);

                /** Execution 1 hour start **/
                L1World.getInstance().broadcastServerMessage("After two minutes, the draw ends.");
                L1World.getInstance().broadcastServerMessage("To take part in Hell lottery, type > [.joinHell]");
                Thread.sleep(60000L); // wait 2 minutes
                L1World.getInstance().broadcastServerMessage("After one minute, the draw ends.");
                L1World.getInstance().broadcastServerMessage("To take part in Hell lottery, type > [.joinHell]");
                Thread.sleep(60000L); // wait 2 minutes
                if (_Members.size() <= 4) {
                    L1World.getInstance().broadcastServerMessage("Not enough participants for Hell.  Lottery to Hell has been cancelled.");
                    setRaidJoin(false); // end of participation time
                    _Members.clear();
                } else {
                    setRaidJoin(false); // end of participation time
                    Choice(); // selected 3 players (lottery)
                }
                Thread.sleep(2080000L); //3800000L 1hr 10 minutes
                Boss();
                Thread.sleep(1500000L); //3800000L 1hr 10 minutes

                /** 1時間後に領土マップのユーザー町に **/
                for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
                    if (pc.getMapId() == 666) {
                        new L1Teleport().teleport(pc, 33970, 33246, (short) 4, pc.getMoveState().getHeading(), true);
                        setHellStart(false);
                    }
                }

                /** 5s later teleport to village again **/
                Thread.sleep(5000L);
                close();
                TelePort5();
                setHellStart(false);

                /** 5s later teleport to village again **/
                Thread.sleep(5000L);
                TelePort6();
                setHellStart(false);

                /** exit message output **/
                End();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Choice() {
        Random rnd1 = new Random();
        Random rnd2 = new Random();
        Random rnd3 = new Random();
        int Cmem1 = rnd1.nextInt(getMemberCount());
        int Cmem2 = rnd2.nextInt(getMemberCount());
        int Cmem3 = rnd3.nextInt(getMemberCount());
        RaidList = getMemberArray();
        RaidList[Cmem1].sendPackets(new S_SystemMessage("Congraulation! You're going to Hell!"));
        RaidList[Cmem1].getInventory().storeItem(42050, 1);
        RaidList[Cmem2].sendPackets(new S_SystemMessage("Congraulation! You're going to Hell!"));
        RaidList[Cmem2].getInventory().storeItem(42050, 1);
        RaidList[Cmem3].sendPackets(new S_SystemMessage("Congraulation! You're going to Hell!"));
        RaidList[Cmem3].getInventory().storeItem(42050, 1);
    }


    public void AddMember(L1PcInstance pc) {
        if (!_Members.contains(pc)) {
            _Members.add(pc);
            pc.sendPackets(new S_SystemMessage("\\aDLottery will begin shortly."));
        }
    }

    public void removeMember(L1PcInstance pc) {
        _Members.remove(pc);
    }

    public void clearMembers() {
        _Members.clear();
    }

    public boolean isMember(L1PcInstance pc) {
        return _Members.contains(pc);
    }

    public L1PcInstance[] getMemberArray() {
        return _Members.toArray(new L1PcInstance[_Members.size()]);
    }

    public int getMemberCount() {
        return _Members.size();
    }

    /**
     * Bring open time
     *
     * @return (Strind）open time（MM-dd HH：mm）
     */
    public String HellOpen() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(sTime);
        return ss.format(c.getTime());
    }

    private void Boss() {
        L1SpawnUtil.spawn2(32807, 32731, (short) 666, 40173, 0, 600 * 1000, 0);
        L1World.getInstance().broadcastServerMessage("\\aDThe nine-tailed fox who rules the hell woke up.");
        L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                "The nine-tailed fox who rules the hell woke up."));
    }
    /**
     *領土が開いていることを確認
     *
     *@return (boolean)開いている場合true閉じている場合false
     */
    // 開催時間を決めて与える他の場所のアプリケーション参照すること！
            /* private boolean isOpen() {
                  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);
				  if ((hour == 03 && minute == 00) || (hour == 11 && minute == 00)
				   || (hour == 19 && minute == 00)) {
				   return true;
				  }
				  return false;
				 }*/


    /**
     * 領土が開いていることを確認
     *
     * @return (boolean) 開いている場合true閉じている場合false
     */
    private boolean isOpen6() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % HELLTIME) == 0)
            return true;
        return false;
    }

    public boolean isOpen7() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % HELLTIME) >= 2
                && (Integer.parseInt(NowTime) % HELLTIME) <= 8)
            return true;
        return false;
    }


    /**
     * Actually bring the current time
     *
     * @return (String) current time（HH：mm）
     */
    private String getTime() {
        return s.format(Calendar.getInstance().getTime());
    }

    /**
     * If a character dies, it will be terminated.
     **/
    private void close() {
        for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
            if (pc.getMap().getId() == 666 && pc.isDead()) {
                pc.stopHpRegenerationByDoll();
                pc.stopMpRegenerationByDoll();
                pc.sendPackets(new S_Disconnect());
            }
        }
    }

    /**
     * teleport to aden village
     **/
    private void TelePort5() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 666:
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 33970, 33246, (short) 4, c.getMoveState().getHeading(), true);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * aden village
     **/
    private void TelePort6() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 666:
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 33970, 33246, (short) 4, c.getMoveState().getHeading(), true);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * End
     **/
    public void End() {
        L1World.getInstance().broadcastServerMessage("The flames of hell have vanished. will be open again in 3 hours.");
        L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "The flames of hell have vanished. will be open again in 3 hours."));
        setHellStart(false);
    }
}