package l1j.server.GameSystem.AStar;

//******************************************************************************
// File Name	: Node.java
// Description	: Node class
// Create		: 2003/04/01 JongHa Woo
// Update		:
//******************************************************************************

public class Node {
    public int f; // f = g+h
    public int h; // Heuristic Value
    public int g; // Distance to Date
    public int x, y; // Node Position
    public Node prev; // Previous Node
    public Node direct[]; // Neighboring Nodes
    public Node next; // Next Node

    // *************************************************************************
    // Name : Node()
    // Desc : Constructor
    // *************************************************************************
    Node() {
        direct = new Node[8];

        for (int i = 0; i < 8; i++) {
            direct[i] = null;
        }
    }

    public void close() {
        for (int i = 0; i < 8; i++) {
            direct[i] = null;
        }
        prev = next = null;
    }

    public void clear() {
        f = 0;
        h = 0;
        g = 0;
        x = 0;
        y = 0;
        prev = null;
        direct = null;
        next = null;
    }
}
