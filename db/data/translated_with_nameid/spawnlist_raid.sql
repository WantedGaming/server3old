INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (1, 1, 'アンタラス', 900011, 32786, 32690, 0);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (2, 0, '雑貨商人^イルバギル', 900009, 32685, 32662, 5);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (3, 0, 'パミリド', 900010, 32681, 32680, 5);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (4, 2, 'ベアサム', 900042, 32934, 32680, 5);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (5, 2, '雑貨商人^カイムサム', 900041, 32920, 32664, 5);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (6, 3, 'パプリオン', 900038, 32956, 32833, 0);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (7, 0, 'クレイ', 7200026, 32689, 32667, 5);
INSERT INTO `spawnlist_raid`(`id`, `type`, `location`, `npc_id`, `locx`, `locy`, `heading`) VALUES (8, 2, '呪われた巫女 サエル', 4039009, 32928, 32661, 4);
